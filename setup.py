import setuptools, sys

sys.path.insert(1, 'simple-python-application')
from _version import __version__, __name__, __description__, __author__, __author_email__

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="{}-pkg-Serhii-Petrychenko".format(__name__),
    version=__version__,
    author=__author__,
    author_email=__author_email__,
    description=__description__,
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/javatar-pro-cd/simple-python-application",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
