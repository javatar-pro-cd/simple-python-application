import sys, unittest, json
sys.path.insert(1, 'simple-python-application')
from app import app

class AppInfoTestCase(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_appInfo(self):
        response = self.app.get("/actuator/info")
        self.assertEqual(response.status_code, 200)

    def test_appHealth(self):
        response = self.app.get("/actuator/health")
        self.assertEqual(response.status_code, 200)

    def test_appNotExisting(self):
        response = self.app.get("/actuator/not-found")
        self.assertEqual(response.status_code, 404)
