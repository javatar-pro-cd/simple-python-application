import sys, unittest, json
sys.path.insert(1, 'simple-python-application')
from app import app


class UsersTestCase(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_get_users_list(self):
        response = self.app.get("/v1/users/")
        self.assertEqual(response.status_code, 200)

    def test_get_user_by_name(self):
        response = self.app.get("/v1/users/Serg")
        resp_json = response.get_json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(resp_json["name"], "Serg")
        self.assertEqual(resp_json["age"], 35)

    def test_create_user(self):
        user = dict(name='Alex', age=39)
        response = self.app.post("/v1/users/", data=json.dumps(user), content_type='application/json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers.getlist("Location")[0], "/v1/users/Alex")

    def test_update_user(self):
        user = dict(name="Alexander", age=38)
        response = self.app.put("/v1/users/Alex", data=json.dumps(user), content_type='application/json')
        resp_json = response.get_json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(resp_json["name"], "Alexander")
        self.assertEqual(int(resp_json["age"]), 38)

    def test_delete_user(self):
        response = self.app.delete("/v1/users/Alexander")
        self.assertEqual(response.status_code, 204)
