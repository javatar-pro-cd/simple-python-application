__name__ = "Simple python application"
__description__ = "This is simple python app for deploying it with jenkins pipeline"
__version__ = "0.0.6"
__author__ = "Serhii Petrychenko"
__author_email__ = "serhii.petrychenko@javatar.pro"

appInfo = {
    "app": {
        "name": __name__,
        "description": __description__,
        "version": __version__,
        "author": {
            "name": __author__,
            "email": __author_email__
        }
    }
}
