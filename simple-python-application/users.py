from flask import make_response
from flask_restful import Resource, reqparse

users = [{"name": "Serg", "age": 35}, {"name": "Julia", "age": 26}, {"name": "Mark", "age": 28}]

class UserListController(Resource):
    def get(self):
        return users, 200

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        parser.add_argument("age")
        args = parser.parse_args()
        name = args["name"]

        for user in users:
            if (name == user["name"]):
                return "User with name {} already exists".format(name), 400

        user = {
            "name": name,
            "age": args["age"],
        }
        users.append(user)
        location = "/v1/users/{}".format(name)
        response = make_response(user, 201, {"Location": location})
        response.autocorrect_location_header = False
        return response

class UserController(Resource):
    def get(self, name):
        for user in users:
            if (name == user["name"]):
                return user, 200
        return "User with name {} not found".format(name), 404

    def put(self, name):
        parser = reqparse.RequestParser()
        parser.add_argument("age")
        parser.add_argument("name")
        args = parser.parse_args()

        for user in users:
            if (name == user["name"]):
                user["age"] = args["age"]
                user["name"] = args["name"]
                return user, 200

        user = {
            "name": args["name"],
            "age": args["age"],
        }
        users.append(user)
        return user, 201

    def delete(self, name):
        global users
        users = [user for user in users if user["name"] != name]
        return "{} is deleted.".format(name), 204
