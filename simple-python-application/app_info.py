from flask_restful import Resource

from _version import appInfo

health = {"status": "UP"}

class ActuatorController(Resource):
    def get(self, name):
        if name == "info":
            return appInfo, 200
        if name == "health":
            return health, 200
        return "404" ,404
