from flask import Flask
from flask_restful import Api
from users import UserListController, UserController
from app_info import ActuatorController

app = Flask(__name__)
api = Api(app)

api.add_resource(ActuatorController, "/actuator/<string:name>")
api.add_resource(UserListController, "/v1/users/")
api.add_resource(UserController, "/v1/users/<string:name>")
