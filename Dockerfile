FROM python:3.8.0-slim

ENV SERVER_PORT 5000

WORKDIR /opt/simple-python-application

COPY env/prod .
COPY simple-python-application .
COPY requirements.txt .

RUN apt-get clean \
    && apt-get -y update

RUN apt-get -y install nginx \
    && apt-get -y install python3-dev \
    && apt-get -y install build-essential

RUN pip install -r requirements.txt --src /usr/local/src

COPY env/prod/nginx.conf /etc/nginx

RUN chmod +x ./start.sh

CMD ["./start.sh"]
